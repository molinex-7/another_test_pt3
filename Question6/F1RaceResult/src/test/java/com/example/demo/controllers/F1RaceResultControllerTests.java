package com.example.demo.controllers;

import java.net.URI;
import java.net.URISyntaxException;

import com.example.demo.domain.models.Circuit;
import com.example.demo.domain.models.Constructor;
import com.example.demo.domain.models.Driver;
import com.example.demo.domain.models.FastestLap;
import com.example.demo.domain.models.Location;
import com.example.demo.domain.models.Races;
import com.example.demo.domain.models.Result;
import com.example.demo.domain.models.Time;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class F1RaceResultControllerTests {
    @LocalServerPort
    int randomServerPort;
     
    @Test
    public void testGetAllSuccess() throws URISyntaxException {
        ResponseEntity<Races> result = new RestTemplate().getForEntity(
                new URI(getEndPoint("/all")), Races.class);

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody() instanceof Races);
    }

    @Test
    public void testGetCircuitSuccess() throws URISyntaxException {
        ResponseEntity<Circuit> result = new RestTemplate().getForEntity(
                new URI(getEndPoint("/circuit")), Circuit.class);

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody() instanceof Circuit);
    }

    @Test
    public void testGetLocationSuccess() throws URISyntaxException {
        ResponseEntity<Location> result = new RestTemplate().getForEntity(
                new URI(getEndPoint("/circuit/location")), Location.class);

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody() instanceof Location);
    }
    
    @Test
    public void testGetResultSuccess() throws URISyntaxException {
        ResponseEntity<Result> result = new RestTemplate().getForEntity(
                new URI(getEndPoint("/per_position/10")), Result.class);

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody() instanceof Result);
    }

    @Test
    public void testGetNoResultException() throws URISyntaxException {
        try {
            new RestTemplate().getForEntity(
                new URI(getEndPoint("/per_position/69")), Result.class);

            Assert.fail();
        } catch (HttpClientErrorException err) {
            Assert.assertEquals(404, err.getRawStatusCode());
            Assert.assertEquals(true, err
                    .getResponseBodyAsString()
                    .contains("Result in position 69 not found. Position must be between 1 and"));
        }
    }

    @Test
    public void testGetDriverSuccess() throws URISyntaxException {
        ResponseEntity<Driver> result = new RestTemplate().getForEntity(
                new URI(getEndPoint("/per_position/10/driver")), Driver.class);

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody() instanceof Driver);
    }

    @Test
    public void testGetConstructorSuccess() throws URISyntaxException {
        ResponseEntity<Constructor> result = new RestTemplate().getForEntity(
                new URI(getEndPoint("/per_position/10/constructor")), Constructor.class);

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody() instanceof Constructor);
    }

    @Test
    public void testGetTimeSuccess() throws URISyntaxException {
        ResponseEntity<Time> result = new RestTemplate().getForEntity(
                new URI(getEndPoint("/per_position/1/time")), Time.class);
        
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody() instanceof Time);
    }

    @Test
    public void testGetTimeNoResultException() throws URISyntaxException {
        try {
            new RestTemplate().getForEntity(
                new URI(getEndPoint("/per_position/10/time")), Time.class);

            Assert.fail();
        } catch (HttpClientErrorException err) {
            Assert.assertEquals(404, err.getRawStatusCode());
            Assert.assertEquals(true, err
                    .getResponseBodyAsString()
                    .contains("Time for 10th position, are not avaliable"));
        }
    }

    @Test
    public void testGetFastestLapSuccess() throws URISyntaxException {
        ResponseEntity<FastestLap> result = new RestTemplate().getForEntity(
                new URI(getEndPoint("/per_position/10/fastest_lap")), FastestLap.class);
        
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody() instanceof FastestLap);
    }

    private String getEndPoint(String endpoint) {
        return "http://localhost:" 
            + randomServerPort 
            + "/f1_race_result/api/last_result/" 
            + endpoint;
    }
}
