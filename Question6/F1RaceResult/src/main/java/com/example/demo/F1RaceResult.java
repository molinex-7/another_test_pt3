package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class F1RaceResult {

	public static void main(String[] args) {
		SpringApplication.run(F1RaceResult.class, args);
	}

}
