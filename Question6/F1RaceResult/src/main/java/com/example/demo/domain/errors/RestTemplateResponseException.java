package com.example.demo.domain.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestTemplateResponseException extends RuntimeException {
    private Logger logger = LoggerFactory.getLogger(RestTemplateResponseException.class);

    public RestTemplateResponseException(String error) {
        super(error);
        logger.error(error);
    }
}

