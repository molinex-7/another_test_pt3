package com.example.demo.domain.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Races {
    @ApiModelProperty(notes="Year of season")
    private int season;
    @ApiModelProperty(notes="Race round")
    private int round;
    @ApiModelProperty(notes="Wiki url with information about race")
    private String url;
    @ApiModelProperty(notes="Race name")
    private String raceName;
    @ApiModelProperty(notes="Day of race")
    private String date;
    @ApiModelProperty(notes="Time of race")
    private String time;
    @ApiModelProperty(notes="Informations about circuit")
    @JsonProperty("Circuit")
    private Circuit circuit;
    @JsonProperty("Results")
    @ApiModelProperty(notes="Informations about result")
    private List<Result> result;
}

