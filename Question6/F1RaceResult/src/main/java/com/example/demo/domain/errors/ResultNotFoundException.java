package com.example.demo.domain.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResultNotFoundException extends RuntimeException {
    private Logger logger = LoggerFactory.getLogger(RestTemplateResponseException.class);

    public ResultNotFoundException(String error) {
        super(error);
        logger.error(error);
    }
}

