package com.example.demo.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Driver {
    @ApiModelProperty(notes="Driver Id")
    private String driverId;
    @ApiModelProperty(notes="Permanent Number")
    private int permanentNumber;
    @ApiModelProperty(notes="Driver code")
    private String code;
    @ApiModelProperty(notes="Url about Driver")
    private String url;
    @ApiModelProperty(notes="Given Name")
    private String givenName;
    @ApiModelProperty(notes="Family Name")
    private String familyName;
    @ApiModelProperty(notes="Date of Birth")
    private String dateOfBirth;
    @ApiModelProperty(notes="Driver nationality")
    private String nationality;
}

