package com.example.demo.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Circuit {
    @ApiModelProperty(notes="Circuit Indentifier")
    private String circuitId;
    @ApiModelProperty(notes="Wiki url with information about circuit")
    private String url;
    @ApiModelProperty(notes="Circuit name")
    private String circuitName;
    @ApiModelProperty(notes="Informations about Location")
    @JsonProperty("Location")
    private Location location;
}

