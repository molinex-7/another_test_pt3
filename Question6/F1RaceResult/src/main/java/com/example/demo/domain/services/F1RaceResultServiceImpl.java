package com.example.demo.domain.services;

import com.example.demo.domain.errors.ResultNotFoundException;
import com.example.demo.domain.interfaces.services.F1RaceResultService;
import com.example.demo.domain.models.Circuit;
import com.example.demo.domain.models.Constructor;
import com.example.demo.domain.models.Driver;
import com.example.demo.domain.models.FastestLap;
import com.example.demo.domain.models.Location;
import com.example.demo.domain.models.Races;
import com.example.demo.domain.models.Result;
import com.example.demo.domain.models.Time;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class F1RaceResultServiceImpl 
    implements F1RaceResultService {
    @Value("${remoteResourceUrl}")
    private String remoteResourceUrl;
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Races getAll() 
            throws JsonMappingException, JsonProcessingException {
        return getRacesConsumingAndParseExternalResources();
    }

    @Override
    public Circuit getCircuit() 
            throws JsonMappingException, JsonProcessingException {
        return getAll().getCircuit();
    }

    @Override
    public Location getLocation() 
            throws JsonMappingException, JsonProcessingException {
        return getCircuit().getLocation();
    }

    @Override
    public Result getResult(int position) 
            throws JsonMappingException, JsonProcessingException {
        int size = getAll().getResult().size();

        if (position < 0 || position > size)
            throw new ResultNotFoundException(
                    "Result in position " 
                    + position 
                    + " not found. Position must be between 1 and " 
                    + size);

        return getAll().getResult().get(position - 1);
    }

    @Override
    public Driver getDriver(int position) 
            throws JsonMappingException, JsonProcessingException {
        return getResult(position).getDriver();
    }

    @Override
    public Constructor getConstructor(int position) 
            throws JsonMappingException, JsonProcessingException {
        return getResult(position).getConstructor();
    }

    @Override
    public Time getTime(int position)
            throws JsonMappingException, JsonProcessingException {
        Time time = getResult(position).getTime();

        if (time == null)
            throw new ResultNotFoundException(
                    "Time for " 
                    + position 
                    + "th position, are not avaliable.");

        return time;
    }

    @Override
    public FastestLap getFastestLap(int position) 
            throws JsonMappingException, JsonProcessingException {
        return getResult(position).getFastestLap();
    }

    private Races getRacesConsumingAndParseExternalResources() 
            throws JsonMappingException, JsonProcessingException {
        JsonNode json = restTemplate.getForObject(remoteResourceUrl, JsonNode.class);
        JsonNode racesNode = json.at("/MRData/RaceTable/Races").get(0);

        return new ObjectMapper().readValue(racesNode.toString(), Races.class);
    }
}

