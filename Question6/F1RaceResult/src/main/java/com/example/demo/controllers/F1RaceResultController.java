package com.example.demo.controllers;

import com.example.demo.domain.interfaces.services.F1RaceResultService;
import com.example.demo.domain.models.Circuit;
import com.example.demo.domain.models.Constructor;
import com.example.demo.domain.models.Driver;
import com.example.demo.domain.models.FastestLap;
import com.example.demo.domain.models.Location;
import com.example.demo.domain.models.Races;
import com.example.demo.domain.models.Result;
import com.example.demo.domain.models.Time;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@Api(tags = "LastResult", description = "API to see result of last race on 2017")
@RequestMapping("/last_result")
@RequiredArgsConstructor
@RestController
@CrossOrigin
public class F1RaceResultController {
    private final F1RaceResultService f1RaceResultServiceImpl;

    @ApiOperation(value = "Return all information about race")
    @GetMapping(path = "/all", produces = "application/json")
    public Races getAll() 
            throws JsonMappingException, JsonProcessingException {
        return f1RaceResultServiceImpl.getAll();
    }

    @ApiOperation(value = "Return information about circuit")
    @GetMapping(path = "/circuit", produces = "application/json")
    public Circuit getCircuit() 
            throws JsonMappingException, JsonProcessingException {
        return f1RaceResultServiceImpl.getCircuit();
    }

    @ApiOperation(value = "Return information about circuit location")
    @GetMapping(path = "/circuit/location", produces = "application/json")
    public Location  getLocation() 
            throws JsonMappingException, JsonProcessingException {
        return f1RaceResultServiceImpl.getLocation();
    }

    @ApiOperation(value = "Return information about which Driver")
    @GetMapping(path = "/per_position/{position}", produces = "application/json")
    public Result getResult(@PathVariable int position) 
            throws JsonMappingException, JsonProcessingException {
        return f1RaceResultServiceImpl.getResult(position);
    }

    @ApiOperation(value = "Return information about Driver")
    @GetMapping(path = "/per_position/{position}/driver", produces = "application/json")
    public Driver getDriver(@PathVariable int position) 
            throws JsonMappingException, JsonProcessingException {
        return f1RaceResultServiceImpl.getDriver(position);
    }

    @ApiOperation(value = "Return information about Constructor")
    @GetMapping(path = "/per_position/{position}/constructor", produces = "application/json")
    public Constructor getConstructor(@PathVariable int position) 
            throws JsonMappingException, JsonProcessingException {
        return f1RaceResultServiceImpl.getConstructor(position);
    }

    @ApiOperation(value = "Return information about Time")
    @GetMapping(path = "/per_position/{position}/time", produces = "application/json")
    public Time getTime(@PathVariable int position) 
            throws JsonMappingException, JsonProcessingException {
        return f1RaceResultServiceImpl.getTime(position);
    }

    @ApiOperation(value = "Return information about Fastest Lap")
    @GetMapping(path = "/per_position/{position}/fastest_lap", produces = "application/json")
    public FastestLap getFastestLap(@PathVariable int position) 
            throws JsonMappingException, JsonProcessingException {
        return f1RaceResultServiceImpl.getFastestLap(position);
    }
}

