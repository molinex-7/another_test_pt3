package com.example.demo.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Location {
    @ApiModelProperty(notes="Latitude of race")
    private String lat;
    @ApiModelProperty(notes="Longetude of race")
    @JsonProperty("long")
    private String longt;
    @ApiModelProperty(notes="Locality of race")
    private String locality;
    @ApiModelProperty(notes="Country of race")
    private String country;
}

