package com.example.demo.domain.errors;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

@Component
public class RestTemplateResponseErrorHandler 
    implements ResponseErrorHandler {
    private final String genericMsg = "External Resource Server Error. Code: ";

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) 
        throws IOException {
        if (httpResponse.getStatusCode().is4xxClientError()
            || httpResponse.getStatusCode().is5xxServerError())
            return true;

        return false;
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) 
        throws IOException {
        throw new RestTemplateResponseException(
                this.genericMsg 
                + httpResponse.getStatusCode().toString());
    }
}
