package com.example.demo.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Time {
    @ApiModelProperty(notes="Millis")
    private Long millis;
    @ApiModelProperty(notes="time")
    @JsonProperty("time")
    private String timeField;
}

