package com.example.demo.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Result {
    @ApiModelProperty(notes="Number")
    private int number;
    @ApiModelProperty(notes="Position")
    private int position;
    @ApiModelProperty(notes="Position in text format")
    private String positionText;
    @ApiModelProperty(notes="Points")
    private int points;
    @ApiModelProperty(notes="Driver")
    @JsonProperty("Driver")
    private Driver driver;
    @ApiModelProperty(notes="Constructor")
    @JsonProperty("Constructor")
    private Constructor constructor;
    @ApiModelProperty(notes="Grid")
    private int grid;
    @ApiModelProperty(notes="Laps")
    private int laps;
    @ApiModelProperty(notes="Status")
    private String status;
    @ApiModelProperty(notes="Time")
    @JsonProperty("Time")
    private Time time;
    @ApiModelProperty(notes="Fastest Lap")
    @JsonProperty("FastestLap")
    private FastestLap fastestLap;
}

