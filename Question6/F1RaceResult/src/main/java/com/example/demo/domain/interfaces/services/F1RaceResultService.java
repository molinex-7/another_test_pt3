package com.example.demo.domain.interfaces.services;

import com.example.demo.domain.models.Circuit;
import com.example.demo.domain.models.Constructor;
import com.example.demo.domain.models.Driver;
import com.example.demo.domain.models.FastestLap;
import com.example.demo.domain.models.Location;
import com.example.demo.domain.models.Races;
import com.example.demo.domain.models.Result;
import com.example.demo.domain.models.Time;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface F1RaceResultService {
    public Races getAll()
        throws JsonMappingException, JsonProcessingException;
    public Circuit getCircuit()
        throws JsonMappingException, JsonProcessingException;
    public Location getLocation()
        throws JsonMappingException, JsonProcessingException;
    public Result getResult(int position)
        throws JsonMappingException, JsonProcessingException;
    public Driver getDriver(int position)
        throws JsonMappingException, JsonProcessingException;
    public Constructor getConstructor(int position)
        throws JsonMappingException, JsonProcessingException;
    public Time getTime(int position)
        throws JsonMappingException, JsonProcessingException;
    public FastestLap getFastestLap(int position)
        throws JsonMappingException, JsonProcessingException;
}

