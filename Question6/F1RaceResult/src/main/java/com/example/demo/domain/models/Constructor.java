package com.example.demo.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Constructor {
    @ApiModelProperty(notes="Constructor Id")
    private String constructorId;
    @ApiModelProperty(notes="Url about constructor")
    private String url;
    @ApiModelProperty(notes="Constructor name")
    private String name;
    @ApiModelProperty(notes="Constructor nationality")
    private String nationality;
}

