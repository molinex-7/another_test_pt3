package com.example.demo.domain.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AverageSpeed {
    @ApiModelProperty(notes="Unit pattern")
    private String units;
    @ApiModelProperty(notes="Average Speed")
    private String speed;
}

