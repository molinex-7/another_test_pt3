package com.example.demo.domain.configs;

import java.time.Duration;

import com.example.demo.domain.errors.RestTemplateResponseErrorHandler;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
            .setConnectTimeout(Duration.ofMillis(3000))
            .setReadTimeout(Duration.ofMillis(3000))
            .errorHandler(new RestTemplateResponseErrorHandler())
            .build();
    }
}
