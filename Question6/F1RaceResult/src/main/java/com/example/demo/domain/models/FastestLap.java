package com.example.demo.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class FastestLap {
    @ApiModelProperty(notes="Rank")
    private int rank;
    @ApiModelProperty(notes="Lap")
    private int lap;
    @ApiModelProperty(notes="Time")
    @JsonProperty("Time")
    private Time time;
    @ApiModelProperty(notes="Average Speed")
    @JsonProperty("AverageSpeed")
    private AverageSpeed averageSpeed;
}

