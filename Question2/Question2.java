public class Question2 {
    private int angryMarvinBirds(String[] result) {
        int i, total = 0;

        for (i = 0; i < result.length; i++)
            total += convertSimbolToNumber(result[i]);

        return total;
    }

    private int convertSimbolToNumber(String simbol) {
        int num = 0;

        num = (simbol.equals("*__"))? 1: num;
        num = (simbol.equals("**_"))? 2: num;
        num = (simbol.equals("***"))? 3: num;

        return num;
    }

    public static void main(String[] args) {
        int total = new Question2().angryMarvinBirds(args);

        System.out.println("Stars total is: " + total);
    }
}
