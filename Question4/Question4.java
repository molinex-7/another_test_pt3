public class Question4 {
    private int[] points;

    private void weAreTheChampions(int[] wins, int[] ties) {
        if (wins.length != ties.length)
            return ;

        this.points = new int[wins.length];
        int champs = 0;

        sumTeamPoints(wins, ties);
        champs = getIndexOfChamps();

        System.out.println("Winner Team has index: " + champs);
        System.out.println("And this Team has " + this.points[champs] + " points.");
    }

    private void sumTeamPoints(int[] wins, int[] ties) {
        int i;

        for (i = 0; i < wins.length; i++) {
            this.points[i] = 0;
            this.points[i] += wins[i] * 3;
            this.points[i] += ties[i] * 1;
        }
    }

    private int getIndexOfChamps() {
        int highPoints = this.points[0];
        int highIndex = 0;
        int i;

        for (i = 1; i < this.points.length; i++) {
            if (this.points[i] > highPoints) {
                highPoints = this.points[i];
                highIndex = i;
            }
        }
        
        return highIndex;
    }

    public static void main(String[] args) {
        int[] wins = {1,0,3};
        int[] ties = {2,2,0};

        new Question4().weAreTheChampions(wins, ties);
    }
}
