public class Question3 {
    private int creepyHours(String[] hours) {
        int i, beeps = 0;

        for (i = 0; i < hours.length; i++)
            beeps += verifyPatern(hours[i]);

        return beeps;
    }

    private int verifyPatern(String hour) {
        int num = 0;

        num = (isShodaiPatern(hour))? 1: num;
        num = (isNidaimePatern(hour))? 1: num;
        num = (isSandaimePatern(hour))? 1: num;
        num = (isYondaimePatern(hour))? 1: num;

        return num;
    }

    private boolean isShodaiPatern(String hour) {
        return hour.charAt(0) == hour.charAt(3)
            && hour.charAt(1) == hour.charAt(4);
    }

    private boolean isNidaimePatern(String hour) {
        return hour.charAt(0) == hour.charAt(1)
            && hour.charAt(3) == hour.charAt(4);
    }

    private boolean isSandaimePatern(String hour) {
        return hour.charAt(0) == hour.charAt(4)
            && hour.charAt(1) == hour.charAt(3);
    }

    private boolean isYondaimePatern(String hour) {
        return hour.charAt(0) == hour.charAt(1)
            && hour.charAt(3) == hour.charAt(4)
            && hour.charAt(0) == hour.charAt(4);
    }

    public static void main(String[] args) {
        int beeps = new Question3().creepyHours(args);

        System.out.println("Clock beeps " + beeps + " times.");
    }
}
