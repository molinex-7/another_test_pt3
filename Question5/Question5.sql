--If tables are normalized 

SELECT 
    v.OrderID as OrderID,
    sum(p.orderPrice) as TotalPrice
FROM VentingTheCosts as v
JOIN OrderTable as p ON p.OrderID = v.OrderID
WHERE v.CustomerName = "DeathStar";
