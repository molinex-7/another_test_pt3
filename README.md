# Another test pt3

Guess what?
Another job test...
In what language?
The one I love the most... Java...

## What we use here

Made by Thiago Molina with technologies like Spring, Junit + Mockito,
Swagger,lombok, Maven, NeoVim(so almighty), curl, openJDK-11, etc...

The kit to make java minimally tolerable. And who says this is the guy, 
who is 1 year and a half tinkering with legacy systems. And you know 
how brutal a legacy project, without a framework, without a pattern, 
can be to the programmer (First reason I want to change jobs).

I owed a container to throw all this inside. I explain below why

### Description

It had been a while since I had auditioned for a job. But this one I decided to do, 
first to complete the trilogy of tests in java (browse here in my gitlab, to see the 
previous ones). Second, this test struck me as interesting. And third, I built a PC 
for Christmas last year, and to this day, I still haven't been able to build my 
development environment on it. My current job consumes so much of my time that 
I can't use the new machine, so this is a good opportunity. While doing the test, 
I take the opportunity to set up my environment and configure it (Second reason I 
want to change jobs. I need time, to enjoy my stuffs).

### Run it 

The first 4 exercises are very similar to those exercises we do in college. And 
they are arranged as follows:

    Question1/
    ├── Question1.java
    ├── Response1.png
    └── Statement1.png

Where:

**Statement1.png:** It's a print with the purpose of the test, the statement that I must solve.

**Question1.java:** It is the implementation of the exercise solution

**Response1.png:** It is the evidence of what has been done

This file is there for 3 reasons:

First, as simple as the program is, it needs to be tested, and its functionality needs to be 
evidenced. When you say: It doesn't matter if it doesn't compile, what matters is the logic. 
It scares me. If it has not been tested, and it is not evidenced, it is not complete. Which means, 
someone will have to complete the task. And I warn you, it won't be me

Second, some of these programs take input arguments. The print shows what they are, and how 
they should be passed

Third, UNIX shell. All my work is done in a web browser, and in a UNIX shell. So it doesn't matter 
if it's OSX, BSD, GNU, and ultimately WSL, I need a shell to work with. As I understand it, the 
working machine is a Mac. I need sudo, brew, and full access to the repositories, in order to set 
up my environment. If it is not possible, please disregard me

The fifth exercise is a database query. It is arranged as follows:

    Question5/
    ├── Question5.sql
    └── Statement5.png

We don't have the evidence print, this query has not been tested, and I don't even know if it works. 
I didn't mock a database to test...

That's the danger in opening precedence...

The sixth exercise asked for a small program that consumes an API. Here is the smallest possible 
program that does the task:

    curl -v 'https://ergast.com/api/f1/2017/last/results.json' | jq '.MRData.RaceTable.Races[0].Results[9].Driver'

I love you Julio Neves. No, no, I'm kidding. I understood that it was to build an API, which would 
consume and display the available resource. Using what I would, in the real world. So for that 
I used Spring

    Question6
    ├── F1RaceResult
    ├── Response6_docs.png
    ├── Response6_test.png
    └── Statement6.png

In the F1RaceResult directory we have a Spring application that does the job. Well, the idea here was to put all 
this in a container, so I don't fill my system with crazy jars. As we know, the docker to be almost dead 
(Already lost support in kubernets). And my machine / OS are brand new. So I thought about the podman. But I 
wouldn't have time, to install, configure, and learn how to use it, while doing this test, so I was owed...

For that same reason, I still used maven. The idea was to use the gradle. But I didn't have time to learn how to use it.

And speaking of maven. Run the tests:

    mvn -Dtest=F1RaceResultControllerTests test

![Screenshot](Question6/Response6_test.png)

Okay, now go up the application

    mvn spring-boot:run

If everything went well, go to:


http://localhost:8080/f1_race_result/api/swagger-ui.html

And you should see a page like this in your browser:

![Screenshot](Question6/Response6_docs.png)

The seventh exercise is available as:

    Question7/
    ├── Question7.txt
    ├── Statement7_pt1.png
    ├── Statement7_pt2.png
    └── Statement7_pt3.png

In the file **Question7.txt**, I have considerations about this exercise

I think that's it. It was a good test, I enjoyed doing it, and I hope you like it. Thanks

### Tip

One time at http://start.spring.io

There they create archetypes for spring designs. If you want to create
one from scratch...

enjoy

### Contact

For any clarification contact us

    Mail: t.molinex@gmail.com


