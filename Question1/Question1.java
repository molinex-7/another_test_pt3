public class Question1 {
    private void fizzingNBuzzing() {
        int i;

        for (i = 1; i < 101; i++)
            System.out.print(numberOrKey(i) + "\t");
    }

    private String numberOrKey(int num) {
        String whatPrint = String.valueOf(num);

        whatPrint = (num % 3 == 0)? "Fizz": whatPrint;
        whatPrint = (num % 5 == 0)? "Buzz": whatPrint;
        whatPrint = (num % 3 == 0 && num % 5 == 0)? "FizzBuzz": whatPrint;

        return whatPrint;
    }

    public static void main(String[] args) {
        new Question1().fizzingNBuzzing();
    }
}
